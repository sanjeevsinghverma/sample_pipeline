# Program to show various ways to read and 
# write data in a file. 
from stackapi import StackAPI
SITE = StackAPI('stackoverflow')
comments = SITE.fetch('comments')
print(comments)
file1 = open("build/myfile.txt","w") 
  
# \n is placed to indicate EOL (End of Line) 
file1.writelines(str(comments))

file1.close() #to change file access modes